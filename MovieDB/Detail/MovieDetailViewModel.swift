//
//  MovieDetailViewModel.swift
//  MovieDB
//
//  Created by Deiby Toralva on 11/30/22.
//

import Foundation

class MovieDetailViewModel {
  
  private let movieService: MovieService
  
  var movie: Movie? {
    didSet {
      self.operationSuccess?()
    }
  }
  
  var operationSuccess: (() -> Void)?
  var errorMessage: ((String) -> Void)?
  
  init(movieId: Int) {
    self.movieService = MovieService(movieId: movieId)
  }
  
  func getData() {
    movieService.getData { result in
      switch result {
      case .success(let movie):
        self.movie = movie
      case .failure(let error):
        self.errorMessage?(error.localizedDescription)
      }
    }
  }
}
