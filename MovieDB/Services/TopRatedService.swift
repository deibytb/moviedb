//
//  TopRatedService.swift
//  MovieDB
//
//  Created by Deiby Toralva on 11/26/22.
//

import Foundation

class TopRatedService {
  
  let service = MovieDBService()
  
  func getData(completion: @escaping (Result<[Movie], MovieDBService.APIError>) -> ()) {
    guard let url = MovieDBService.Service.topRated.url else {
      completion(.failure(.parseUrl))
      return
    }
    
    URLSession.shared.dataTask(with: url) { (data, response, error) in
      guard let data = data else {
        completion(.failure(MovieDBService.APIError.noResponse))
        return
      }
      guard error == nil else {
        completion(.failure(.networkError(error: error!)))
        return
      }
      print(data)
      do {
        let jsonDecoder = JSONDecoder()
        jsonDecoder.keyDecodingStrategy = .convertFromSnakeCase
        let topRatedResponse = try jsonDecoder.decode(TopRatedResults.self, from: data)
        completion(.success(topRatedResponse.results))
      } catch let error {
        completion(.failure(.jsonDecodingError(error: error)))
      }
    }.resume()
  }
}
