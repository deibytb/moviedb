//
//  HCollectionTableViewCell.swift
//  MovieDB
//
//  Created by Deiby Toralva on 11/26/22.
//

import UIKit

protocol HCollectionDelegate: AnyObject {
  func openMovieDetail(with movie: Movie)
}

extension HCollectionTableViewCell {
  static let identifier = "HCollectionTableViewCell"
}

class HCollectionTableViewCell: UITableViewCell {
  
  @IBOutlet weak private var collectionView: UICollectionView!
  
  weak var delegate: HCollectionDelegate?
  
  var movies: [Movie] = [] {
    didSet {
      DispatchQueue.main.async {
        self.collectionView.reloadData()
      }
    }
  }
  
  override func awakeFromNib() {
    super.awakeFromNib()
    
    self.collectionView.dataSource = self
    self.collectionView.delegate = self
  }
}

extension HCollectionTableViewCell: UICollectionViewDataSource {
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return self.movies.count
  }
  
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: MovieCollectionViewCell.identifier, for: indexPath) as? MovieCollectionViewCell else {
      return UICollectionViewCell()
    }
    cell.movie = self.movies[indexPath.row]
    return cell
  }
}

extension HCollectionTableViewCell: UICollectionViewDelegate {
  func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    let movie = movies[indexPath.row]
    self.delegate?.openMovieDetail(with: movie)
  }
}
