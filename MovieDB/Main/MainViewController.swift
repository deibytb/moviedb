//
//  ViewController.swift
//  MovieDB
//
//  Created by Deiby Toralva on 11/26/22.
//

import UIKit

class MainViewController: UIViewController {
  
  @IBOutlet weak private var tableView: UITableView!
  
  var viewModel = MainViewModel()
  
  var sections: [MainViewModel.Section] = [] {
    didSet {
      DispatchQueue.main.async {
        self.tableView.reloadData()
      }
    }
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    self.tableView.dataSource = self
    self.tableView.delegate = self
    
    self.setupBindings()
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)  
    
    self.viewModel.getData()
  }
  
  private func setupBindings() {
    self.viewModel.operationSuccess = {
      self.sections = self.viewModel.sections
    }
    
    self.viewModel.errorMessage = { error in
      // Show error message
      print(error)
    }
  }
  
  func showMovieDetailVC(with movie: Movie) {
    let storyBoard = UIStoryboard(name: "MovieDetail", bundle: nil)
    if let movieDetailVC = storyBoard.instantiateInitialViewController() as? MovieDetailViewController {
      movieDetailVC.viewModel = MovieDetailViewModel(movieId: movie.id)
      self.present(movieDetailVC, animated: true)
    }
  }
}

extension MainViewController: UITableViewDataSource {
  func numberOfSections(in tableView: UITableView) -> Int {
    return self.sections.count
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    switch self.sections[section] {
    case .collection: return 1
    case .movies(_, let movies): return movies.count
    }
  }
  
  func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
    switch self.sections[section] {
    case .collection(title: let title, movies: _): return title
    case .movies(title: let title, movies: _): return title
    }
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    switch self.sections[indexPath.section] {
    case .collection(title: _, movies: let movies):
      guard let cell = tableView.dequeueReusableCell(withIdentifier: HCollectionTableViewCell.identifier, for: indexPath) as? HCollectionTableViewCell else {
        return UITableViewCell()
      }
      cell.delegate = self
      cell.movies = movies
      return cell
    case .movies(title: _, movies: let movies):
      guard let cell = tableView.dequeueReusableCell(withIdentifier: MovieTableViewCell.identifier, for: indexPath) as? MovieTableViewCell else {
        return UITableViewCell()
      }
      cell.movie = movies[indexPath.row]
      return cell
    }
  }
}

extension MainViewController: UITableViewDelegate {
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    switch self.sections[indexPath.section] {
    case .movies(title: _, movies: let movies):
      let movie = movies[indexPath.row]
      self.showMovieDetailVC(with: movie)
    default:
      return
    }
  }
}

extension MainViewController: HCollectionDelegate {
  func openMovieDetail(with movie: Movie) {
    self.showMovieDetailVC(with: movie)
  }
}
