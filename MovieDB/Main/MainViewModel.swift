//
//  MainViewModel.swift
//  MovieDB
//
//  Created by Deiby Toralva on 11/26/22.
//

import Foundation

class MainViewModel {
  
  enum Section {
    case collection(title: String, movies: [Movie])
    case movies(title: String, movies: [Movie])
  }
  
  private let topRatedService = TopRatedService()
  private let popularService = PopularService()
  
  var sections: [MainViewModel.Section] = [] {
    didSet {
      self.operationSuccess?()
    }
  }
  
  var operationSuccess: (() -> Void)?
  var errorMessage: ((String) -> Void)?
  
  func getData() {
    let group = DispatchGroup()
    group.enter()
    self.getTopRatedMovies {
      group.leave()
    }
    group.enter()
    self.getPopularMovies {
      group.leave()
    }
  }
  
  private func getTopRatedMovies(completion: (() -> Void)) {
    topRatedService.getData { result in
      switch result {
      case .success(let movies):
        let collection = Section.collection(title: "Top Rated", movies: movies)
        self.sections.append(collection)
      case .failure(let error):
        self.errorMessage?(error.localizedDescription)
      }
    }
  }
  
  private func getPopularMovies(completion: (() -> Void)) {
    popularService.getData { result in
      switch result {
      case .success(let movies):
        let movies = Section.movies(title: "Popular movies", movies: movies)
        self.sections.append(movies)
      case .failure(let error):
        self.errorMessage?(error.localizedDescription)
      }
    }
  }
}
