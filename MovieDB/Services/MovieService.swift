//
//  MovieService.swift
//  MovieDB
//
//  Created by Deiby Toralva on 11/30/22.
//

import Foundation

class MovieService {
  
  let service = MovieDBService()
  
  let movieId: Int
  
  init(movieId: Int) {
    self.movieId = movieId
  }
  
  func getData(completion: @escaping (Result<Movie, MovieDBService.APIError>) -> ()) {
    guard let url = MovieDBService.Service.movie(id: movieId).url else {
      completion(.failure(.parseUrl))
      return
    }
    
    URLSession.shared.dataTask(with: url) { (data, response, error) in
      guard let data = data else {
        completion(.failure(MovieDBService.APIError.noResponse))
        return
      }
      guard error == nil else {
        completion(.failure(.networkError(error: error!)))
        return
      }
      
      do {
        let jsonDecoder = JSONDecoder()
        jsonDecoder.keyDecodingStrategy = .convertFromSnakeCase
        let movieResponse = try jsonDecoder.decode(Movie.self, from: data)
        completion(.success(movieResponse))
      } catch let error {
        completion(.failure(.jsonDecodingError(error: error)))
      }
    }.resume()
  }
}
