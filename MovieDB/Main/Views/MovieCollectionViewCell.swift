//
//  MoviewCollectionViewCell.swift
//  MovieDB
//
//  Created by Deiby Toralva on 11/26/22.
//

import UIKit
import Kingfisher

extension MovieCollectionViewCell {
  static let identifier = "MovieCollectionViewCell"
}

class MovieCollectionViewCell: UICollectionViewCell {
  
  @IBOutlet weak private var thumnailView: UIImageView!
  @IBOutlet weak private var nameLabel: UILabel!
  
  var movie: Movie? {
    didSet {
      self.updateUI()
    }
  }
  
  override func awakeFromNib() {
    super.awakeFromNib()
    // Config cell
  }
  
  private func updateUI() {
    guard let movie = self.movie else {
      return
    }
    
    DispatchQueue.main.async {
      self.nameLabel.text = movie.title
      self.thumnailView.kf.setImage(with: movie.getImageURL())
    }
  }
}
