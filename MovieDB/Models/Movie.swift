//
//  Movie.swift
//  MovieDB
//
//  Created by Deiby Toralva on 11/26/22.
//

import Foundation

struct TopRatedResults: Decodable {
  let results: [Movie]
}

struct Movie: Decodable {
  var id: Int
  var title: String?
  var posterPath: String?
  var releaseDate: String?
  var voteAverage: Double?
  var voteCount: Int?
  
  func getImageURL() -> URL? {
    guard let path = posterPath else {
      return nil
    }
    return ImageService(path: path).getURL()
  }
}
