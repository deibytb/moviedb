//
//  ImageService.swift
//  MovieDB
//
//  Created by Deiby Toralva on 11/26/22.
//

import Foundation

struct ImageService {
  
  let path: String
  let width: Int = 400
  
  init(path: String) {
    self.path = path
  }
  
  func getURL() -> URL? {
    return URL(string: "https://image.tmdb.org/t/p/w\(width)\(path)")
  }
}
