//
//  MovieDBService.swift
//  MovieDB
//
//  Created by Deiby Toralva on 11/26/22.
//

import Foundation

class MovieDBService {
  
  static let baseURL: String = "https://api.themoviedb.org"
  static let apiKeyComponent = URLQueryItem(name: "api_key", value: "da46c2f935630aa5ce86bad1d154870c")
  
  enum APIError: Error {
    case parseUrl
    case noResponse
    case jsonDecodingError(error: Error)
    case networkError(error: Error)
  }
  
  enum Service {
    case topRated
    case popular
    case movie(id: Int)
    
    var url: URL? {
      var components = URLComponents(string: MovieDBService.baseURL)
      components?.queryItems = [MovieDBService.apiKeyComponent]
      
      switch self {
      case .topRated:
        components?.path = "/3/movie/top_rated"
      case .popular:
        components?.path = "/3/movie/popular"
      case .movie(id: let id):
        components?.path = "/3/movie/\(id)"
      }
      return components?.url
    }
  }
}
