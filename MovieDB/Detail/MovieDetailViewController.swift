//
//  MovieDetailViewController.swift
//  MovieDB
//
//  Created by Deiby Toralva on 11/30/22.
//

import UIKit

class MovieDetailViewController: UIViewController {
  
  @IBOutlet weak private var thumnailView: UIImageView!
  @IBOutlet weak private var nameLabel: UILabel!
  @IBOutlet weak private var releaseDateLabel: UILabel!
  @IBOutlet weak private var voteAverageLabel: UILabel!
  @IBOutlet weak private var voteCountLabel: UILabel!
  
  var viewModel: MovieDetailViewModel?
  
  var movie: Movie? {
    didSet {
      self.updateUI()
    }
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    self.setupBindings()
    
    self.nameLabel.text = ""
    self.releaseDateLabel.text = ""
    self.voteAverageLabel.text = ""
    self.voteCountLabel.text = ""
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    
    self.viewModel?.getData()
  }
  
  private func setupBindings() {
    self.viewModel?.operationSuccess = {
      self.movie = self.viewModel?.movie
    }
    
    self.viewModel?.errorMessage = { error in
      // Show error message
      print(error)
    }
  }
  
  private func updateUI() {
    guard let movie = self.movie else {
      return
    }
    
    DispatchQueue.main.async {
      self.thumnailView.kf.setImage(with: movie.getImageURL())
      self.nameLabel.text = movie.title
      self.releaseDateLabel.text = "Release date: " + (movie.releaseDate ?? "")
      self.voteAverageLabel.text = "Vote average: " +  (movie.voteAverage?.description ?? "")
      self.voteCountLabel.text = "Vote count: " + (movie.voteCount?.description ?? "")
    }
  }
}
